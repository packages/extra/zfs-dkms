# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>

# Arch credits:
# Maintainer: Kevin Stolp <kevinstolp@gmail.com>
# Contributor: Eli Schwartz <eschwartz@archlinux.org>
# Contributor: Iacopo Isimbaldi <isiachi@rhye.it>

pkgname=zfs-dkms
pkgver=2.2.7
pkgrel=2
pkgdesc="Kernel modules for the Zettabyte File System."
arch=('x86_64')
url="https://zfsonlinux.org/"
license=('CDDL-1.0')
depends=("zfs-utils=${pkgver}" 'dkms')
provides=("ZFS-MODULE=${pkgver}")
# ambiguous, provided for backwards compat, pls don't use
provides+=('zfs')
source=("https://github.com/zfsonlinux/zfs/releases/download/zfs-${pkgver/_/-}/zfs-${pkgver/_/-}.tar.gz"{,.asc}
        '0001-only-build-the-module-in-dkms.conf.patch'
        'https://github.com/openzfs/zfs/pull/16666/commits/3488cdb9ba5397780a9fc349678942d203a1b7de.patch'
        'https://github.com/openzfs/zfs/pull/17026.patch')
sha256sums=('b2b8e3bfabf2a6407a0132243726cb6762547a5bd095b1b1f37eaf2a9d8f7672'
            'SKIP'
            '8d5c31f883a906ab42776dcda79b6c89f904d8f356ade0dab5491578a6af55a5'
            'ba39d3f7fcb9509977f41bf17efeffe4d03a2e674b4813547f71331c244fd1b8'
            '36b341a6191b1a5c519b427020a4186036375e8b8d1e9d59a824fb1e3941b326')
b2sums=('31a066d5d543e3328d1afd065a8da4f9b4c52433d585cdbb41936900fd1453431585c6bc4b6e6d9a497a03e79969bae883e17cf4677099686feb3e32577d2777'
        'SKIP'
        '58dc2494e71b50833d44c126b72acad52e9817626542afbc245b7ba82009e8c8252ebde6023592aac42d9942207e7655c0a421da9067fbdd619746ebc372d791'
        'd70578b853a48e86add9f2436c7fcd9a40fe8fccc5f80f95ed04a3063f7db0276a931e0ba506a711453fd6947f4216a52ebb4a174978e31f14193392dc3d9b77'
        'c2483e69d3329b82511cb77f2d769f9bb09c8f88c2450b4662c30dea74707e4a9fbc3582af4c8f2d09b1f4442800c08f23dedf644e045ff0e6d4b655e64cc431')
validpgpkeys=('4F3BA9AB6D1F8D683DC2DFB56AD860EED4598027'  # Tony Hutter (GPG key for signing ZFS releases) <hutter2@llnl.gov>
              'C33DF142657ED1F7C328A2960AB9E991C6AF658B') # Brian Behlendorf <behlendorf1@llnl.gov>

prepare() {
  cd "${pkgname%-dkms}-${pkgver}"

  patch -p1 -i ../0001-only-build-the-module-in-dkms.conf.patch
  patch -p1 -i ../3488cdb9ba5397780a9fc349678942d203a1b7de.patch
  patch -p1 -i ../17026.patch

  # remove unneeded sections from module build
  sed -ri "/AC_CONFIG_FILES/,/]\)/{
/AC_CONFIG_FILES/n
/]\)/n
/^\s*(module\/.*|${pkgname%-dkms}.release|Makefile)/!d
}" configure.ac

  autoreconf -fi
}

build() {
  cd "${pkgname%-dkms}-${pkgver}"

  ./scripts/dkms.mkconf -n ${pkgname%-dkms} -v ${pkgver} -f dkms.conf
  ./scripts/make_gitrev.sh include/zfs_gitrev.h
}

package() {
  cd "${pkgname%-dkms}-${pkgver}"
  local dkmsdir="${pkgdir}/usr/src/${pkgname%-dkms}-${pkgver}"

  install -d "${dkmsdir}"/{config,scripts}
  cp -a configure dkms.conf Makefile.in META ${pkgname%-dkms}_config.h.in ${pkgname%-dkms}.release.in include/ module/ "${dkmsdir}"/
  cp config/compile config/config.* config/missing config/*sh "${dkmsdir}"/config/
  
  # permissions and delete unwanted files
  chmod 644 "${dkmsdir}"/config/config.*
  chmod 755 "${dkmsdir}"/config/*sh
  #rm -v "${dkmsdir}"/config/config.*~
  cp scripts/dkms.postbuild "${dkmsdir}"/scripts/
}
